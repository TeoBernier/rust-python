use super::prelude::*;
use std::ops::{Deref, DerefMut};

#[derive(Debug)]
pub struct Sequences(Vec<(String, Vec<Nucleotide>)>);

impl Default for Sequences {
    fn default() -> Self {
        Self::new(Vec::new())
    }
}

impl Sequences {
    pub fn new(sequences: Vec<(String, Vec<Nucleotide>)>) -> Self {
        Sequences(sequences)
    }
}

impl Deref for Sequences {
    type Target = Vec<(String, Vec<Nucleotide>)>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Sequences {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
