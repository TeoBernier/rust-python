pub mod genes;
pub mod nucleotide;
pub mod sequence;
pub mod sequences;



pub mod prelude {
    // pub use super::confusion_matrix::ConfusionMatrix;
    // pub use super::diff_list::DiffList;
    pub use super::genes::{Gene, GenesList};
    pub use super::nucleotide::Nucleotide;
    pub use super::sequence::Sequence;
    // pub use super::sequence_result::SequenceResult;
    pub use super::sequences::Sequences;
}