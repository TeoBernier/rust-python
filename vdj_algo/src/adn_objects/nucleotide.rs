#[allow(unused_imports)]
use super::prelude::*;
use std::cmp::PartialEq;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum Nucleotide {
    A = 'a' as isize,
    C = 'c' as isize,
    G = 'g' as isize,
    T = 't' as isize,
    N = 'n' as isize,
}

impl Nucleotide {
    pub fn as_index(self) -> usize {
        use Nucleotide::*;
        match self {
            A => 0,
            C => 1,
            G => 2,
            T => 3,
            N => panic!("Unknown nucleotide : N !"),
        }
    }
}

use std::convert::From;

impl From<char> for Nucleotide {
    fn from(ch: char) -> Nucleotide {
        use Nucleotide::*;
        match ch {
            'A' | 'a' => A,
            'C' | 'c' => C,
            'G' | 'g' => G,
            'T' | 't' => T,
            'N' | 'n' => N,
            _ => panic!("Invalid character : {:?}", ch),
        }
    }
}

use std::convert::Into;
impl Into<char> for Nucleotide {
    fn into(self) -> char {
        use Nucleotide::*;
        match self {
            A => 'A',
            C => 'C',
            G => 'G',
            T => 'T',
            N => 'N',
        }
    }
}
