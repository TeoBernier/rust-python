use super::prelude::*;
use std::fmt;
use std::slice::SliceIndex;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub struct Sequence<'a>(&'a [Nucleotide]);

impl<'a> Sequence<'a> {
    pub fn new_nucleotides(s: &str) -> Vec<Nucleotide> {
        s.chars()
            .filter_map(|ch| match ch {
                '.' | '\r' | '\n' | ' ' => None, //ignore .
                _ => Some(Nucleotide::from(ch)),
            })
            .collect::<Vec<_>>()
    }

    pub fn from(nucleotides: &[Nucleotide]) -> Sequence {
        Sequence(nucleotides)
    }

    pub fn slice(&self, index: impl SliceIndex<[Nucleotide], Output = [Nucleotide]>) -> Sequence {
        Sequence(&self.0[index])
    }

    // pub fn get_best_alignment(gene: &[Nucleotide], sequence: &[Nucleotide]) -> (i32, String) {
    //     let mut result = vec![(0, 2); (sequence.len() + 1) * (gene.len() + 1)];
    //     let size = gene.len() + 1;
    //     for (j, res) in result.iter_mut().enumerate().take(size) {
    //         *res = (-2 * j as i32, 0);
    //     }

    //     for i in 1..=sequence.len() {
    //         for j in 1..gene.len() {
    //             let skip = (result[(i - 1) * size + j].0 - 3, 2);
    //             let skip_seq = (result[i * size + j - 1].0 - 3, 0);
    //             let step = (
    //                 result[(i - 1) * size + j - 1].0
    //                     + if sequence[sequence.len() - i] == gene[gene.len() - j] {
    //                         2
    //                     } else {
    //                         -4
    //                     },
    //                 1,
    //             );
    //             result[i * size + j] = skip.max(skip_seq.max(step));
    //         }
    //         let j = gene.len();

    //         let skip = (result[(i - 1) * size + j].0, 2);
    //         let skip_seq = (result[i * size + j - 1].0 - 3, 0);
    //         let step = (
    //             result[(i - 1) * size + j - 1].0
    //                 + if sequence[sequence.len() - i] == gene[gene.len() - j] {
    //                     2
    //                 } else {
    //                     -4
    //                 },
    //             1,
    //         );
    //         result[i * size + j] = skip.max(skip_seq.max(step));
    //     }

    //     let mut i = sequence.len();
    //     let mut j = gene.len();
    //     let mut s = String::new();

    //     while i != 0 || j != 0 {
    //         match result[i * size + j].1 {
    //             2 => {
    //                 s.push('.');
    //                 i -= 1;
    //             }
    //             1 => {
    //                 s.push(gene[gene.len() - j] as isize as u8 as char);
    //                 j -= 1;
    //                 i -= 1;
    //             }
    //             0 => {
    //                 j -= 1;
    //             }
    //             _ => unreachable!(),
    //         }
    //     }
    //     (result[result.len() - 1].0, s)
    // }
}

use std::ops::Deref;

impl<'a> Deref for Sequence<'a> {
    type Target = [Nucleotide];
    fn deref(&self) -> &Self::Target {
        self.0
    }
}

impl<'a> fmt::Display for Sequence<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "{}",
            self.0
                .iter()
                .map(|&n| n as isize as u8 as char)
                .collect::<String>()
        )
    }
}
