use super::prelude::*;
use std::collections::HashMap;
use std::ops::Deref;
use std::ops::DerefMut;

#[derive(Debug, Clone)]
pub struct Gene {
    pub name: String,
    pub sequence: Vec<Nucleotide>,
}

#[derive(Debug, Clone)]
pub struct GenesList(pub Vec<Gene>);

impl GenesList {
    pub fn new(genes_list: Vec<Gene>) -> GenesList {
        GenesList(genes_list)
    }

    pub fn get_bests<F: Fn(&Gene) -> i32, const N: usize>(&self, f: F) -> GenesList {
        use std::cmp::Reverse;
        let mut score = self.clone();
        score.sort_by_cached_key(|gene| Reverse(f(gene)));
        score.truncate(N);
        score
    }

    pub fn get_map(&self) -> HashMap<String, Vec<Nucleotide>> {
        self.iter()
            .map(|gene| (gene.name.clone(), gene.sequence.clone()))
            .collect()
    }
}

impl Deref for GenesList {
    type Target = Vec<Gene>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for GenesList {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
