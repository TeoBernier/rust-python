use crate::prelude::*;

pub fn load_genes_list_from_file(path: &str) -> GenesList {
    use std::fs::File;
    use std::io::Read;

    let mut file = File::open(path).expect("Cannot open file");
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).expect("Cannot read file");

    GenesList::new(
        buffer
            .split('>')
            .skip(1)
            .map(|s| {
                let mut format_gene = s.split('|');
                Gene {
                    name: format_gene.nth(1).unwrap().to_string(),
                    sequence: Sequence::new_nucleotides(format_gene.last().unwrap()),
                }
            })
            .collect(),
    )
}

pub fn load_sequences_from_file(path: &str) -> Sequences {
    use std::fs::File;
    use std::io::Read;
    let mut file = File::open(path).expect("Cannot open file");

    let mut buffer = String::new();

    file.read_to_string(&mut buffer).expect("Cannot read file");

    Sequences::new(
        buffer
            .split('>')
            .skip(1)
            .map(|s| {
                let mut format_sequence = s.split('\n');
                (
                    format_sequence.next().unwrap().trim().to_string(),
                    Sequence::new_nucleotides(format_sequence.next().unwrap()),
                )
            })
            .collect(),
    )
}

// pub fn load_V_sequences_from_processed_file(path: &str) -> (Sequences, HashMap<String, String>) {
//     use std::fs::File;
//     use std::io::Read;

//     let mut sequences = Sequences::new(vec![]);
//     let mut sequence_result = HashMap::new();

//     let mut file = File::open(path).expect("Cannot open file");

//     let mut buffer = String::new();

//     file.read_to_string(&mut buffer).expect("Cannot read file");

//     for (name, seq, res) in buffer.split('>').skip(1).map(|s| {
//         let mut format_sequence = s.split('\n');
//         let gene_v = format_sequence.next().unwrap();
//         let name = format_sequence.next().unwrap();
//         let seq = Sequence::new_nucleotides(name);
//         (name.to_string(), seq, gene_v)
//     }) {
//         sequences.push((name.clone(), seq));
//         sequence_result.insert(name, res.to_string());
//     }

//     (sequences, sequence_result)
// }

pub fn load_translation_from_file(path: &str) -> Vec<String> {
    use std::fs::File;
    use std::io::Read;
    let mut file = File::open(path).expect("Cannot open file");
    let mut buffer = String::new();

    file.read_to_string(&mut buffer).expect("Cannot read file");

    buffer.split('\n').map(|x| x.to_string()).collect()
}

pub fn load_sequences_from_file_with_mutations(path: &str) -> (Sequences, SequenceResult) {
    let mut sequences = Sequences::new(vec![]);
    let mut sequence_result = SequenceResult::new();
    use std::fs::File;
    use std::io::Read;
    let mut file = File::open(path).expect("Cannot open file");

    let mut buffer = String::new();

    file.read_to_string(&mut buffer).expect("Cannot read file");

    for (name, seq, res) in buffer.split('>').skip(1).map(|s| {
        let mut format_sequence = s.split('\n');
        let mut first_line = format_sequence.next().unwrap().trim().split('|');
        let name = first_line.next().unwrap();
        let gene_v = first_line.next().unwrap().to_string();

        let gene_d = first_line.next().unwrap().to_string();

        let gene_j = first_line.next().unwrap().to_string();

        let seq = Sequence::new_nucleotides(format_sequence.next().unwrap());
        (name.to_string(), seq, (gene_v, gene_d, gene_j))
    }) {
        sequences.push((name.clone(), seq));
        sequence_result.insert(name, res);
    }

    (sequences, sequence_result)
}

pub fn load_sequences_from_real_data_file(path: &str) -> Sequences {
    use std::fs::File;
    use std::io::Read;

    let mut sequences = Sequences::new(vec![]);

    let mut file = File::open(path).expect("Cannot open file");

    let mut buffer = String::new();

    file.read_to_string(&mut buffer).expect("Cannot read file");

    for (res, seq) in buffer.trim().split('\n').map(|s| {
        let mut s = s.split('\t');
        let res = s.next().unwrap();
        let seq = s.next().unwrap();
        let seq = Sequence::new_nucleotides(seq);
        (res.to_string(), seq)
    }) {
        sequences.push((res, seq));
    }

    sequences
}

// pub fn load_hmm_matrices_from_simulations(
//     path: &str,
//     genes_map: &Map,
//     hmm_matrices: &mut HMMGenesMatrices,
// ) {
//     use std::fs::File;
//     use std::io::BufRead;
//     use std::io::BufReader;
//     use Nucleotide::*;

//     let file = File::open(path).expect("Cannot open file");
//     let reader = BufReader::new(file);

//     for line in reader.lines() {
//         let line = line.expect("Cannot read file");
//         let mut iter = line.split('\t');
//         if let (Some(name), Some(seq)) = (iter.next(), iter.next()) {
//             if let Some((&name, _)) = genes_map.get_key_value(name) {
//                 let sequence = Sequence::vec_from_chars(seq);

//                 let (n, map) = hmm_matrices
//                     .0
//                     .entry(name)
//                     .or_insert_with(|| (4, HashMap::new()));
//                 if *n == 4 {
//                     hmm_matrices.1 += 5;
//                 } else {
//                     hmm_matrices.1 += 1;
//                 }
//                 *n += 1;
//                 map.entry(sequence.len())
//                     .or_insert_with(|| vec![[1, 1, 1, 1]; sequence.len()])
//                     .iter_mut()
//                     .zip(sequence.into_iter())
//                     .for_each(|(n_array, n)| match n {
//                         A => n_array[0] += 1,
//                         C => n_array[1] += 1,
//                         G => n_array[2] += 1,
//                         T => n_array[3] += 1,
//                         _ => (),
//                     });
//             }
//         }
//     }
// }

pub fn load_genes_pattern_from_simulations(
    path: &str,
    genes_pattern: &mut HashMap<(Vec<Nucleotide>, String), u32>,
) {
    use std::fs::File;
    use std::io::BufRead;
    use std::io::BufReader;

    let file = File::open(path).expect("Cannot open file");
    let reader = BufReader::new(file);

    for line in reader.lines() {
        let line = line.expect("Cannot read file");
        let mut iter = line.split('\t');
        if let (Some(name), Some(seq)) = (iter.next(), iter.next()) {
            let sequence = Sequence::new_nucleotides(seq);

            *genes_pattern
                .entry((sequence, name.to_string()))
                .or_insert(1) += 1;
        }
    }
}

pub fn load_pattern_from_simulations(
    path: &str,
    genes_pattern: &mut HashMap<(Vec<Nucleotide>, String), u32>,
) {
    use std::fs::File;
    use std::io::BufRead;
    use std::io::BufReader;

    let file = File::open(path).expect("Cannot open file");
    let reader = BufReader::new(file);

    for line in reader.lines() {
        let line = line.expect("Cannot read file");
        let mut iter = line.split('\t');
        if let (Some(name), Some(seq)) = (iter.next(), iter.next()) {
            let sequence = Sequence::new_nucleotides(seq);

            *genes_pattern
                .entry((sequence, name.to_string()))
                .or_insert(1) += 1;
        }
    }
}
