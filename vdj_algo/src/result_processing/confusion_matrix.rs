// use crate::adn_objects::prelude::*;
// use core::ops::Deref;
// use std::collections::hash_map::Entry::{Occupied, Vacant};
// use std::collections::HashMap;
// use std::fmt;

// pub struct ConfusionMatrix(Vec<Vec<usize>>, HashMap<&'static str, usize>);

// impl ConfusionMatrix {
//     pub fn new(genes: &GenesList) -> ConfusionMatrix {
//         let (size, hash_index) = genes.iter().fold(
//             (0, HashMap::new()),
//             |(idx, mut hash_map), gene| match hash_map.entry(gene.name) {
//                 Occupied(..) => (idx, hash_map),

//                 entry @ Vacant(..) => {
//                     entry.insert(idx);
//                     (idx + 1, hash_map)
//                 }
//             },
//         );
//         let confusion = vec![vec![0; size]; size];
//         ConfusionMatrix(confusion, hash_index)
//     }

//     pub fn new_without_allele(genes: &GenesList) -> ConfusionMatrix {
//         let (size, hash_index) = genes.deref().clone().into_iter().fold(
//             (0, HashMap::new()),
//             |(idx, mut hash_map), gene| {
//                 if hash_map.contains_key(gene.name.split('*').next().unwrap()) {
//                     (idx, hash_map)
//                 } else {
//                     hash_map.insert(gene.name.split('*').next().unwrap(), idx);
//                     (idx + 1, hash_map)
//                 }
//             },
//         );
//         let confusion = vec![vec![0; size]; size];
//         ConfusionMatrix(confusion, hash_index)
//     }

//     pub fn get_ids(&self) -> Vec<&'static str> {
//         let mut res = self.1.iter().map(|(&k, &v)| (v, k)).collect::<Vec<_>>();
//         res.sort_by_key(|(a, _)| *a);
//         res.into_iter().map(|(_, k)| k).collect()
//     }

//     pub fn increment(&mut self, prediction: &str, result: &str) {
//         if let Some(&idx) = self.1.get(result) {
//             let i = idx;
//             if let Some(&idx) = self.1.get(prediction) {
//                 let j = idx;
//                 self.0[i][j] += 1;
//             }
//         }
//     }

//     pub fn increment_without_allele(&mut self, prediction: &str, result: &str) {
//         if let Some(&idx) = self.1.get(result.split('*').next().unwrap()) {
//             let i = idx;
//             if let Some(&idx) = self.1.get(prediction.split('*').next().unwrap()) {
//                 let j = idx;
//                 self.0[i][j] += 1;
//             }
//         }
//     }
// }

// impl Deref for ConfusionMatrix {
//     type Target = [Vec<usize>];

//     fn deref(&self) -> &Self::Target {
//         &self.0
//     }
// }

// impl fmt::Display for ConfusionMatrix {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(
//             f,
//             "{}",
//             self.0
//                 .iter()
//                 .map(|line| format!("{:?}\n", line))
//                 .collect::<String>()
//         )
//     }
// }
