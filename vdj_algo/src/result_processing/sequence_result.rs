use super::prelude::*;
use crate::adn_objects::prelude::*;
use crate::algorithms::prelude::*;
use std::collections::HashMap;

use std::io::Write;

use std::ops::{Deref, DerefMut};

#[derive(Debug)]
pub struct SequenceResult(HashMap<String, (String, String, String)>);

impl Default for SequenceResult {
    fn default() -> Self {
        Self::new()
    }
}

// pub type Map = HashMap<&'static str, Sequence>;
impl SequenceResult {
    pub fn new() -> Self {
        SequenceResult(HashMap::new())
    }
    pub fn load_from_file(path: &str) -> Self {
        use std::fs::File;
        use std::io::Read;
        let mut file = File::open(path).expect("Cannot open file");

        let mut buffer = String::new();

        file.read_to_string(&mut buffer).expect("Cannot read file");

        SequenceResult(
            buffer
                .split('\n')
                .rev()
                .skip(1)
                .map(|s| {
                    let mut format_result = s.trim().split('\t');
                    (
                        format_result
                            .next()
                            .expect("Problem reading formated date !")
                            .to_string(),
                        (
                            format_result
                                .next()
                                .expect("Problem reading formated date !")
                                .to_string(),
                            format_result
                                .next()
                                .expect("Problem reading formated date !")
                                .to_string(),
                            format_result
                                .next()
                                .expect("Problem reading formated date !")
                                .to_string(),
                        ),
                    )
                })
                .collect(),
        )
    }

    pub fn save_to(&self, path: &str) {
        use std::fs::File;
        let mut file = File::create(path).expect("Cannot create file");
        file.write_all(
            self.0
                .iter()
                .map(|(key, value)| {
                    format!(
                        "{:8}\t{:20}\t{:20}\t{:20}\n",
                        key, value.0, value.1, value.2
                    )
                })
                .collect::<String>()
                .as_bytes(),
        )
        .expect("Cannot write to file");
    }

    pub fn compare_with(&self, results: &SequenceResult, path: &str) {
        use std::fs::File;
        use std::io::BufWriter;

        let mut writer = BufWriter::new(File::create(path).expect("Cannot create file"));

        let mut keys = self.0.keys().collect::<Vec<_>>();
        keys.sort_by_key(|key| key.strip_prefix("S").unwrap().parse::<usize>().unwrap());

        for key in keys {
            if let (Some(res), Some(pred)) = (results.0.get(key), self.0.get(key)) {
                write!(
                    writer,
                    "{:8}Pred : {:20}{:20}{:20}\n        Res  : {:20}{:20}{:20}\n\n",
                    key, pred.0, pred.1, pred.2, res.0, res.1, res.2
                )
                .expect("Cannot write to file");
            }
        }
    }

    pub fn confusion_with(&self, results: &SequenceResult, path: &str) {
        use std::fs::File;

        let mut file = File::create(path).expect("Cannot create file");

        let (v, d, j, n) =
            self.0
                .iter()
                .fold((0, 0, 0, 0), |(mut v, mut d, mut j, mut n), (key, pred)| {
                    if let Some(res) = results.0.get(key) {
                        if res.0 == pred.0 {
                            v += 1;
                        }
                        if res.1 == pred.1 {
                            d += 1;
                        }
                        if res.2 == pred.2 {
                            j += 1;
                        }
                        n += 1;
                    }
                    (v, d, j, n)
                });

        let v_per = v as f64 / n as f64 * 100.;
        let d_per = d as f64 / n as f64 * 100.;
        let j_per = j as f64 / n as f64 * 100.;
        let tot = v + d + j;
        let tot_per = tot as f64 / (3 * n) as f64 * 100.;

        writeln!(file, "V  P : {:4} | {:3.2}%", v, v_per).expect("Cannot write to file");
        writeln!(file, "   F : {:4} | {:3.2}%\n", n - v, 100. - v_per)
            .expect("Cannot write to file");
        writeln!(file, "D  P : {:4} | {:3.2}%", d, d_per).expect("Cannot write to file");
        writeln!(file, "   F : {:4} | {:3.2}%\n", n - d, 100. - d_per)
            .expect("Cannot write to file");
        writeln!(file, "J  P : {:4} | {:3.2}%", j, j_per).expect("Cannot write to file");
        writeln!(file, "   F : {:4} | {:3.2}%\n", n - j, 100. - j_per)
            .expect("Cannot write to file");
        writeln!(file, "ALL P : {:4} | {:3.2}%", tot, tot_per).expect("Cannot write to file");
        writeln!(
            file,
            "    F : {:4} | {:3.2}%\n",
            3 * n - tot,
            100. - tot_per
        )
        .expect("Cannot write to file");
    }

    pub fn confusion_with_without_allele(&self, results: &SequenceResult, path: &str) {
        use std::fs::File;

        let mut file = File::create(path).expect("Cannot create file");

        let (v, d, j, n) =
            self.0
                .iter()
                .fold((0, 0, 0, 0), |(mut v, mut d, mut j, mut n), (key, pred)| {
                    if let Some(res) = results.0.get(key) {
                        if res.0.split('*').next() == pred.0.split('*').next() {
                            v += 1;
                        }
                        if res.1.split('*').next() == pred.1.split('*').next() {
                            d += 1;
                        }
                        if res.2.split('*').next() == pred.2.split('*').next() {
                            j += 1;
                        }
                        n += 1;
                    }
                    (v, d, j, n)
                });
        let v_per = v as f64 / n as f64 * 100.;
        let d_per = d as f64 / n as f64 * 100.;
        let j_per = j as f64 / n as f64 * 100.;
        let tot = v + d + j;
        let tot_per = tot as f64 / (3 * n) as f64 * 100.;
        writeln!(file, "V  P : {:4} | {:3.2}%", v, v_per).expect("Cannot write to file");
        writeln!(file, "   F : {:4} | {:3.2}%\n", n - v, 100. - v_per)
            .expect("Cannot write to file");
        writeln!(file, "D  P : {:4} | {:3.2}%", d, d_per).expect("Cannot write to file");
        writeln!(file, "   F : {:4} | {:3.2}%\n", n - d, 100. - d_per)
            .expect("Cannot write to file");
        writeln!(file, "J  P : {:4} | {:3.2}%", j, j_per).expect("Cannot write to file");
        writeln!(file, "   F : {:4} | {:3.2}%\n", n - j, 100. - j_per)
            .expect("Cannot write to file");
        writeln!(file, "ALL P : {:4} | {:3.2}%", tot, tot_per).expect("Cannot write to file");
        writeln!(
            file,
            "    F : {:4} | {:3.2}%\n",
            3 * n - tot,
            100. - tot_per
        )
        .expect("Cannot write to file");
    }

    pub fn get_best_by<
        FV_: Fn(&Gene) -> i32,
        FD_: Fn(&Gene) -> i32,
        FJ_: Fn(&Gene) -> i32,
        FV: Fn(Sequence) -> FV_,
        FD: Fn(Sequence) -> FD_,
        FJ: Fn(Sequence) -> FJ_,
    >(
        sequence: &(String, Sequence),
        genes_v: &GenesList,
        genes_d: &GenesList,
        genes_j: &GenesList,
        fv: FV,
        fd: FD,
        fj: FJ,
    ) -> (String, String, String) {
        let max_v = genes_v
            .iter()
            .map(|gene| gene.sequence.len())
            .filter(|&length| length < sequence.1.len())
            .max()
            .unwrap();

        let best_v =
            genes_v.get_bests::<_, 1>(fv(sequence.1.slice(..sequence.1.len().min(max_v + 3))));

        let best_j = genes_j.get_bests::<_, 1>(fj(sequence.1.slice(best_v.len()..)));

        let best_d = genes_d.get_bests::<_, 5>(fd(sequence.1.slice(
            best_v.len().saturating_sub(3)
                ..sequence.1.len().min(sequence.1.len() + 3 - best_j.len()),
        )));
        print!(
            "\r{}", // => Best D : {:?}",
            sequence.0,
            // best_d.iter().map(|gene| gene.name).collect::<Vec<_>>()
        );

        (
            best_v[0].name.clone(),
            best_d[0].name.clone(),
            best_j[0].name.clone(),
        )
    }

    // pub fn get_best_by_alignment(
    //     sequence: &(String, Sequence),
    //     genes_v: &GenesList,
    //     genes_d: &GenesList,
    //     genes_j: &GenesList,
    // ) -> (Gene, Gene, Gene) {
    //     let best_v = genes_v.get_bests::<_, 1>(alignment_scores(sequence.1, match_n_sequence::<3>));

    //     let best_j = genes_j.get_bests::<_, 1>(alignment_scores(
    //         sequence.1.slice(best_v.len()..),
    //         match_whole_sequence,
    //     ));

    //     let best_d = genes_d.get_bests::<_, 1>(alignment_scores(
    //         sequence
    //             .1
    //             .slice(best_v.len()..sequence.1.len() - best_j.len()),
    //         match_d_sequence,
    //     ));

    //     (best_v[0], best_d[0], best_j[0])
    // }

    // pub fn calcul_from(
    //     sequences: &Sequences,
    //     genes_v: &GenesList,
    //     genes_d: &GenesList,
    //     genes_j: &GenesList,
    // ) -> Self {
    //     SequenceResult(
    //         sequences
    //             .iter()
    //             .map(|sequence| {
    //                 //println!("{}", sequence.0);
    //                 let (best_v, best_d, best_j) =
    //                     SequenceResult::get_best_by_alignment(sequence, genes_v, genes_d, genes_j);
    //                 (sequence.0.clone(), (best_v.name, best_d.name, best_j.name))
    //             })
    //             .collect(),
    //     )
    // }

    // pub fn calcul_and_write_to<F, W: Write>(
    //     sequences: &Sequences,
    //     genes_v: &GenesList,
    //     genes_d: &GenesList,
    //     genes_j: &GenesList,
    //     resultats: &SequenceResult,
    //     f: F,
    //     mut w: W,
    // ) -> Self
    // where
    //     F: Fn(&(String, Sequence), &GenesList, &GenesList, &GenesList) -> (Gene, Gene, Gene),
    // {
    //     SequenceResult(
    //         sequences
    //             .iter()
    //             .map(|sequence| {
    //                 print!("\r{}", sequence.0);
    //                 let (best_v, best_d, best_j) = f(sequence, genes_v, genes_d, genes_j);
    //                 let tmp = (sequence.0.clone(), (best_v.name, best_d.name, best_j.name));
    //                 let resultat = resultats
    //                     .0
    //                     .get(&tmp.0)
    //                     .or_else(|| {
    //                         println!("WTF : {}", &tmp.0);
    //                         Some(&("No gene", "No gene", "No gene"))
    //                     })
    //                     .unwrap();
    //                 write!(
    //                     w,
    //                     "{:8}Pred : {:20}{:20}{:20}\n        Res  : {:20}{:20}{:20}\n\n",
    //                     tmp.0, tmp.1 .0, tmp.1 .1, tmp.1 .2, resultat.0, resultat.1, resultat.2,
    //                 )
    //                 .expect("Cannot write !");
    //                 tmp
    //             })
    //             .collect(),
    //     )
    // }

    // pub fn calcul_and_save_to(
    //     path: &str,
    //     sequences: &Sequences,
    //     genes_v: &GenesList,
    //     genes_d: &GenesList,
    //     genes_j: &GenesList,
    //     results: &SequenceResult,
    // ) -> Self {
    //     use std::fs::File;
    //     use std::io::Write;
    //     let mut file = File::create(path).expect("Cannot create file");
    //     SequenceResult(
    //         sequences
    //             .iter()
    //             .map(|sequence| {
    //                 let (best_v, best_d, best_j) =
    //                     SequenceResult::get_best_genes(sequence, genes_v, genes_d, genes_j);
    //                 let tmp = (
    //                     sequence.0.clone(),
    //                     (
    //                         best_v.0.to_string(),
    //                         best_d.0.to_string(),
    //                         best_j.0.to_string(),
    //                     ),
    //                 );
    //                 println!("{}", sequence.0);
    //                 let result = results.0.get(&tmp.0).unwrap();

    //                 let seq = sequence.1.to_string();
    //                 let seq_bytes = seq.as_bytes();

    //                 let v_pred = Sequence::get_best_alignment(
    //                     genes_v.get_gene(&tmp.1 .0).unwrap(),
    //                     &sequence.1,
    //                 )
    //                 .1;

    //                 let d_pred = ('.'..='.').cycle().take(best_v.1 - 10).collect::<String>()
    //                     + &Sequence::get_best_alignment(
    //                         genes_d.get_gene(&tmp.1 .1).unwrap(),
    //                         &sequence.1[best_v.1 - 10..],
    //                     )
    //                     .1;

    //                 let j_pred = ('.'..='.')
    //                     .cycle()
    //                     .take(best_v.1 + best_d.1 - 20)
    //                     .collect::<String>()
    //                     + &Sequence::get_best_alignment(
    //                         genes_j.get_gene(&tmp.1 .2).unwrap(),
    //                         &sequence.1[best_v.1 + best_d.1 - 20..],
    //                     )
    //                     .1;

    //                 let v_res = if let Some(gene) = genes_v.get_gene(&result.0) {
    //                     Sequence::get_best_alignment(gene, &sequence.1).1
    //                 } else {
    //                     ('-'..='-')
    //                         .cycle()
    //                         .take(sequence.1.len())
    //                         .collect::<String>()
    //                 };

    //                 let d_res = if let Some(gene) = genes_d.get_gene(&result.1) {
    //                     Sequence::get_best_alignment(gene, &sequence.1).1
    //                 } else {
    //                     ('-'..='-')
    //                         .cycle()
    //                         .take(sequence.1.len())
    //                         .collect::<String>()
    //                 };

    //                 let j_res = if let Some(gene) = genes_j.get_gene(&result.2) {
    //                     Sequence::get_best_alignment(gene, &sequence.1).1
    //                 } else {
    //                     ('-'..='-')
    //                         .cycle()
    //                         .take(sequence.1.len())
    //                         .collect::<String>()
    //                 };

    //                 let preds: [&str; 3] = [&v_pred, &d_pred, &j_pred];
    //                 let res: [&str; 3] = [&v_res, &d_res, &j_res];

    //                 let check = |i: usize, seqs: [&str; 3]| {
    //                     Ok(true)
    //                         == seqs
    //                             .iter()
    //                             .map(|seq| seq.as_bytes())
    //                             .try_fold(false, |b, s| match s[i] {
    //                                 b'.' => Ok(b),
    //                                 b'-' => Ok(true),
    //                                 c if c == seq_bytes[i] => {
    //                                     if !b {
    //                                         Ok(true)
    //                                     } else {
    //                                         Err(())
    //                                     }
    //                                 }
    //                                 _ => Err(()),
    //                             })
    //                 };

    //                 let indices: Vec<_> = (0..seq_bytes.len())
    //                     .filter(|&i| !check(i, preds) || !check(i, res))
    //                     .collect();

    //                 let seq_to_string = |idx: &[usize], seq: &str| -> String {
    //                     let mut s = if idx[0] > 6 {
    //                         format!("[{}]-{}", idx[0], &seq[idx[0]..=idx[0]])
    //                     } else {
    //                         (&seq[0..=idx[0]]).to_string()
    //                     };

    //                     let mut last_i = idx[0];

    //                     for &i in idx.iter().skip(1) {
    //                         if i > last_i + 7 {
    //                             s += &format!("-[{}]-{}", i - last_i - 1, &seq[i..=i]);
    //                         } else {
    //                             s += &seq[last_i + 1..=i];
    //                         }
    //                         last_i = i;
    //                     }

    //                     if seq.len() - 1 > last_i + 6 {
    //                         s += &format!("-[{}]\n", seq.len() - last_i - 2);
    //                     } else {
    //                         s = s + &seq[last_i + 1..] + "\n";
    //                     };
    //                     s
    //                 };

    //                 if indices.is_empty() {
    //                     file.write_all(
    //                         format!("Sequence : {}\n--Matches Perfectly--\n\n", tmp.0).as_bytes(),
    //                     )
    //                     .expect("Cannot write to file");
    //                 } else {
    //                     file.write_all(
    //                         format!(
    //                             "Sequence : {}\n{}\nPredictions :\n{}\n\nResults :\n{}\n\n\n",
    //                             tmp.0,
    //                             seq_to_string(&indices, &seq),
    //                             preds
    //                                 .iter()
    //                                 .map(|seq| seq_to_string(&indices, seq))
    //                                 .collect::<String>(),
    //                             res.iter()
    //                                 .map(|seq| seq_to_string(&indices, seq))
    //                                 .collect::<String>(),
    //                         )
    //                         .as_bytes(),
    //                     )
    //                     .expect("Cannot write to file");
    //                 }
    //                 tmp
    //             })
    //             .collect(),
    //     )
    // }

    // pub fn get_confusion_matrixes(
    //     predictions: &SequenceResult,
    //     results: &SequenceResult,
    //     genes_v: &GenesList,
    //     genes_d: &GenesList,
    //     genes_j: &GenesList,
    // ) -> (ConfusionMatrix, ConfusionMatrix, ConfusionMatrix) {
    //     let mut confusion_v = ConfusionMatrix::new(genes_v);
    //     let mut confusion_d = ConfusionMatrix::new(genes_d);
    //     let mut confusion_j = ConfusionMatrix::new(genes_j);
    //     predictions.0.iter().for_each(|(name, prediction)| {
    //         let result = results
    //             .0
    //             .get(name)
    //             .expect("Cannot get result for this sequence");
    //         confusion_v.increment(&prediction.0, &result.0);
    //         confusion_d.increment(&prediction.1, &result.1);
    //         confusion_j.increment(&prediction.2, &result.2);
    //     });
    //     (confusion_v, confusion_d, confusion_j)
    // }

    // pub fn get_confusion_matrixes_without_allele(
    //     predictions: &SequenceResult,
    //     results: &SequenceResult,
    //     genes_v: &GenesList,
    //     genes_d: &GenesList,
    //     genes_j: &GenesList,
    // ) -> (ConfusionMatrix, ConfusionMatrix, ConfusionMatrix) {
    //     let mut confusion_v = ConfusionMatrix::new_without_allele(genes_v);
    //     let mut confusion_d = ConfusionMatrix::new_without_allele(genes_d);
    //     let mut confusion_j = ConfusionMatrix::new_without_allele(genes_j);
    //     predictions.0.iter().for_each(|(name, prediction)| {
    //         let result = results
    //             .0
    //             .get(name)
    //             .expect("Cannot get result for this sequence");
    //         confusion_v.increment_without_allele(&prediction.0, &result.0);
    //         confusion_d.increment_without_allele(&prediction.1, &result.1);
    //         confusion_j.increment_without_allele(&prediction.2, &result.2);
    //     });
    //     (confusion_v, confusion_d, confusion_j)
    // }
}

impl Deref for SequenceResult {
    type Target = HashMap<String, (String, String, String)>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for SequenceResult {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
