#![allow(incomplete_features)]
#![feature(const_generics, entry_insert, map_first_last)]

pub mod adn_objects;
pub mod algorithms;
pub mod result_processing;
pub mod utils;

pub mod prelude {
    pub use super::utils::*;
    pub use crate::adn_objects::prelude::*;
    pub use crate::algorithms::prelude::*;
    pub use crate::result_processing::prelude::*;
    pub use std::collections::BTreeMap;
    pub use std::collections::HashMap;
}
