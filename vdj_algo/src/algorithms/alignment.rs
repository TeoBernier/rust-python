use crate::prelude::*;

pub fn match_n_sequence<const N: usize>(gene: Sequence, sequence: Sequence) -> i32 {
    const GENE_DELETION: i32 = 3;
    const GENE_INSERTION: i32 = 3;
    const GENE_MUTATION: i32 = 1;
    const MATCHING_GENE: i32 = 2;
    let mut result = [0; N];
    let offset = N / 2;
    let size = gene.len().min(sequence.len());
    result[0] = (gene.len() - size) as i32 * (-GENE_DELETION);

    for idx_gen in 1..N - offset {
        result[idx_gen] = result[idx_gen - 1] - GENE_INSERTION;
    }

    for idx_seq in 0..=offset {
        result[offset - idx_seq] = result[offset - idx_seq + 1] - GENE_DELETION;
        for idx_gen in offset - idx_seq + 1..N - 1 {
            let insertion = result[idx_gen + 1] - GENE_INSERTION;
            let deletion = result[idx_gen - 1] - GENE_DELETION;
            let matching = result[idx_gen]
                + if gene[idx_seq + idx_gen - offset] == sequence[idx_seq] {
                    MATCHING_GENE
                } else {
                    -GENE_MUTATION
                };
            result[idx_gen] = insertion.max(deletion.max(matching));
        }

        let deletion = result[N - 2] - GENE_DELETION;
        let matching = result[N - 1]
            + if gene[idx_seq + N - 1 - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[N - 1] = deletion.max(matching);
    }

    for idx_seq in offset + 1..=size + offset - N {
        let insertion = result[1] - GENE_INSERTION;
        let matching = result[0]
            + if gene[idx_seq - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[0] = insertion.max(matching);
        for idx_gen in 1..N - 1 {
            let insertion = result[idx_gen + 1] - GENE_INSERTION;
            let deletion = result[idx_gen - 1] - GENE_DELETION;
            let matching = result[idx_gen]
                + if gene[idx_seq + idx_gen - offset] == sequence[idx_seq] {
                    MATCHING_GENE
                } else {
                    -GENE_MUTATION
                };
            result[idx_gen] = insertion.max(deletion.max(matching));
        }

        let deletion = result[N - 2] - GENE_DELETION;
        let matching = result[N - 1]
            + if gene[idx_seq + N - 1 - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[N - 1] = deletion.max(matching);
    }

    for idx_seq in size + offset - N + 1..size {
        let insertion = result[1] - GENE_INSERTION;
        let matching = result[0]
            + if gene[idx_seq - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[0] = insertion.max(matching);
        for idx_gen in 1..size - idx_seq {
            let insertion = result[idx_gen + 1] - GENE_INSERTION;
            let deletion = result[idx_gen - 1] - GENE_DELETION;
            let matching = result[idx_gen]
                + if gene[idx_seq + idx_gen - offset] == sequence[idx_seq] {
                    MATCHING_GENE
                } else {
                    -GENE_MUTATION
                };
            result[idx_gen] = insertion.max(deletion.max(matching));
        }
    }

    result[0]
}

pub fn match_n_sequence_param(gene: Sequence, sequence: Sequence, n: usize) -> i32 {
    const GENE_DELETION: i32 = 3;
    const GENE_INSERTION: i32 = 3;
    const GENE_MUTATION: i32 = 1;
    const MATCHING_GENE: i32 = 2;
    let mut result = vec![0; n];
    let offset = n / 2;
    let size = gene.len().min(sequence.len());
    result[0] = (gene.len() - size) as i32 * (-GENE_DELETION);

    for idx_gen in 1..n - offset {
        result[idx_gen] = result[idx_gen - 1] - GENE_INSERTION;
    }

    for idx_seq in 0..=offset {
        result[offset - idx_seq] = result[offset - idx_seq + 1] - GENE_DELETION;
        for idx_gen in offset - idx_seq + 1..n - 1 {
            let insertion = result[idx_gen + 1] - GENE_INSERTION;
            let deletion = result[idx_gen - 1] - GENE_DELETION;
            let matching = result[idx_gen]
                + if gene[idx_seq + idx_gen - offset] == sequence[idx_seq] {
                    MATCHING_GENE
                } else {
                    -GENE_MUTATION
                };
            result[idx_gen] = insertion.max(deletion.max(matching));
        }

        let deletion = result[n - 2] - GENE_DELETION;
        let matching = result[n - 1]
            + if gene[idx_seq + n - 1 - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[n - 1] = deletion.max(matching);
    }

    for idx_seq in offset + 1..=size + offset - n {
        let insertion = result[1] - GENE_INSERTION;
        let matching = result[0]
            + if gene[idx_seq - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[0] = insertion.max(matching);
        for idx_gen in 1..n - 1 {
            let insertion = result[idx_gen + 1] - GENE_INSERTION;
            let deletion = result[idx_gen - 1] - GENE_DELETION;
            let matching = result[idx_gen]
                + if gene[idx_seq + idx_gen - offset] == sequence[idx_seq] {
                    MATCHING_GENE
                } else {
                    -GENE_MUTATION
                };
            result[idx_gen] = insertion.max(deletion.max(matching));
        }

        let deletion = result[n - 2] - GENE_DELETION;
        let matching = result[n - 1]
            + if gene[idx_seq + n - 1 - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[n - 1] = deletion.max(matching);
    }

    for idx_seq in size + offset - n + 1..size {
        let insertion = result[1] - GENE_INSERTION;
        let matching = result[0]
            + if gene[idx_seq - offset] == sequence[idx_seq] {
                MATCHING_GENE
            } else {
                -GENE_MUTATION
            };
        result[0] = insertion.max(matching);
        for idx_gen in 1..size - idx_seq {
            let insertion = result[idx_gen + 1] - GENE_INSERTION;
            let deletion = result[idx_gen - 1] - GENE_DELETION;
            let matching = result[idx_gen]
                + if gene[idx_seq + idx_gen - offset] == sequence[idx_seq] {
                    MATCHING_GENE
                } else {
                    -GENE_MUTATION
                };
            result[idx_gen] = insertion.max(deletion.max(matching));
        }
    }

    result[0]
}

pub fn match_whole_sequence(gene: Sequence, sequence: Sequence) -> i32 {
    let mut result = vec![0; (sequence.len() + 1) * (gene.len() + 1)];
    let size = gene.len() + 1;
    for (j, res) in result.iter_mut().enumerate().take(size) {
        *res = -2 * j as i32;
    }

    for i in 1..=sequence.len() {
        for j in 1..gene.len() {
            let skip = result[(i - 1) * size + j] - 3;
            let skip_seq = result[i * size + j - 1] - 3;
            let step = result[(i - 1) * size + j - 1]
                + if sequence[sequence.len() - i] == gene[gene.len() - j] {
                    2
                } else {
                    -1
                };
            result[i * size + j] = skip.max(skip_seq.max(step));
        }
        let j = gene.len();

        let skip = result[(i - 1) * size + j];
        let skip_seq = result[i * size + j - 1] - 3;
        let step = result[(i - 1) * size + j - 1]
            + if sequence[sequence.len() - i] == gene[gene.len() - j] {
                2
            } else {
                -1
            };
        result[i * size + j] = skip.max(skip_seq.max(step));
    }

    result[result.len() - 1]
}

pub fn match_d_sequence(gene: Sequence, sequence: Sequence) -> i32 {
    let mut result = vec![0_i32; sequence.len() - gene.len() + 1];

    for i in 0..gene.len() {
        for j in 0..result.len() {
            if sequence[i + j] == gene[i] {
                result[j] += 1;
            } else {
                result[j] -= 2;
            }
        }
    }

    result.into_iter().max().unwrap()
}

pub fn alignment_scores<F: Fn(Sequence, Sequence) -> i32>(
    sequence: Vec<Nucleotide>,
    f: F,
) -> impl Fn(Gene) -> i32 {
    move |gene: Gene| f(Sequence::from(&gene.sequence), Sequence::from(&sequence))
}
