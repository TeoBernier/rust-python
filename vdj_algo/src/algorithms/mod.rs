pub mod alignment;
pub mod hmm;
pub mod k_mers;
pub mod markov_tree;

pub mod prelude {
    pub use super::alignment::*;
    pub use super::hmm::*;
    pub use super::k_mers::*;
    pub use super::markov_tree::*;
    // pub use crate::log_markov_tree;
}
