// use super::super::prelude::*;

// pub struct HMMGenesMatrices(
//     pub HashMap<&'static str, (usize, HashMap<usize, Vec<[usize; 4]>>)>,
//     pub usize,
// );

// // fn match_sequence(sequence: Sequence, matrice: Vec<[usize; 4]>) -> usize {
// //     use Nucleotide::*;
// //     let mut final_prob = 1.;
// //     for prob in matrice
// //         .iter()
// //         .zip(sequence.iter())
// //         .map(|(n_array, n)| match n {
// //             A => n_array[0] as f64 / n_array.iter().sum::<usize>() as f64,
// //             C => n_array[1] as f64 / n_array.iter().sum::<usize>() as f64,
// //             G => n_array[2] as f64 / n_array.iter().sum::<usize>() as f64,
// //             T => n_array[3] as f64 / n_array.iter().sum::<usize>() as f64,
// //             N => 0.25,
// //         })
// //     {
// //         final_prob *= prob;
// //     }
// //     (final_prob * usize::MAX as f64) as usize
// // }
// impl Default for HMMGenesMatrices {
//     fn default() -> Self {
//         Self::new()
//     }
// }

// impl HMMGenesMatrices {
//     pub fn new() -> HMMGenesMatrices {
//         HMMGenesMatrices(HashMap::new(), 0)
//     }

//     pub fn train(&mut self, path: &str, genes_d: &Map) -> &mut Self {
//         load_hmm_matrices_from_simulations(path, genes_d, self);
//         self
//     }

//     // pub fn print_matrice(&self, gene: &str) {
//     //     println!("{} :", gene);
//     //     let (total, matrices) = self.0.get(gene).unwrap();
//     //     for matrice in matrices.1.iter() {
            
//     //     }
//     // }
// }

// // pub fn hmm_scores(sequence: Sequence, matrices: &HMMGenesMatrices) -> impl Fn(Gene) -> i32 + '_ {
// //     move |gene: Gene| {
// //         use Nucleotide::*;
// //         let total = matrices.1;
// //         let (_, matrices) = match matrices.0.get(gene.name) {
// //             Some(mat) => mat,
// //             None => return 0,
// //         };

// //         // let n = *n;

// //         matrices
// //             .iter()
// //             .map(|(&n, matrice)| {
// //                 let mut result = vec![n as f64 / total as f64; sequence.len() - matrice.len() + 1];

// //                 for i in 0..matrice.len() {
// //                     for j in 0..result.len() {
// //                         result[j] *= match sequence[i + j] {
// //                             A => matrice[i][0] as f64 / matrice[i].iter().sum::<usize>() as f64,
// //                             C => matrice[i][1] as f64 / matrice[i].iter().sum::<usize>() as f64,
// //                             G => matrice[i][2] as f64 / matrice[i].iter().sum::<usize>() as f64,
// //                             T => matrice[i][3] as f64 / matrice[i].iter().sum::<usize>() as f64,
// //                             N => 0.25,
// //                         }
// //                     }
// //                 }

// //                 (result.into_iter().fold(f64::NEG_INFINITY, |a, b| a.max(b))
// //                     * (i32::MAX as f64
// //                         * 0.977_f64.powi(sequence.len() as i32 - matrice.len() as i32)))
// //                     as i32
// //             })
// //             .max()
// //             .unwrap_or(0)
// //     }
// // }
