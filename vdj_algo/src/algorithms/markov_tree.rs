// use crate::prelude::*;
// use std::cmp::Ordering;

// use lazy_static::lazy_static;

// #[derive(Clone, Debug)]
// pub struct LogMarkovTree {
//     vertices: Vec<LogVertices>,
//     final_states: HashMap<u32, String>,
//     cmp_proba: f64,
//     cmp_mut_rate: f64,
//     cmp_mut_row_rate: f64,
// }

// use std::collections::BTreeSet;
// struct Search<'a> {
//     id: u32,
//     idx: u16,
//     last_muted: bool,
//     mutation_in_a_row: u8,
//     matching: u16,
//     total: u16,
//     proba: f32,
//     final_state: bool,
//     vertice: u32,
//     tree: &'a LogMarkovTree,
// }

// use std::sync::Mutex;
// lazy_static! {
//     static ref ID: Mutex<u32> = Mutex::new(0);
// }

// use std::fmt::{self, Debug, Formatter};
// impl<'a> Debug for Search<'a> {
//     fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
//         f.debug_struct("Search")
//             .field("idx", &self.idx)
//             .field("matching", &self.matching)
//             .field("mutation_in_a_row", &self.mutation_in_a_row)
//             .field("total", &self.total)
//             .field("proba", &self.proba)
//             .field("final_state", &self.final_state)
//             .field("vertice", &self.vertice)
//             .finish()
//     }
// }

// impl<'a> Search<'a> {
//     fn new(tree: &'a LogMarkovTree) -> Self {
//         let mut id = ID.lock().unwrap();
//         let search = Self {
//             id: *id,
//             tree,
//             last_muted: true,
//             idx: 0,
//             matching: 0,
//             total: 0,
//             vertice: 0,
//             proba: 0.,
//             final_state: false,
//             mutation_in_a_row: 0,
//         };
//         *id += 1;
//         search
//     }

//     fn prob(&self) -> f64 {
//         //Laplace Succession rule
//         let prob_match = (self.matching + 1) as f64 / (self.total + 2) as f64;
//         let prob_mut_row = 1. - (self.mutation_in_a_row + 1) as f64 / (self.total + 2) as f64;
//         prob_match.ln() * self.tree.cmp_mut_rate
//             + self.proba as f64 * self.tree.cmp_proba
//             + prob_mut_row.ln() * self.tree.cmp_mut_row_rate
//     }

//     fn is_final(&self) -> bool {
//         self.final_state
//     }

//     fn final_search(mut self, sequence: &[Nucleotide]) -> Self {
//         self.matching += ((sequence.len() as u16 - self.total) as f64 *  0.4).round() as u16;
//         self.total = sequence.len() as u16;
//         self.final_state = true;
//         self
//     }
// }

// impl<'a> PartialEq for Search<'a> {
//     fn eq(&self, other: &Self) -> bool {
//         self.id == other.id
//     }
// }

// impl<'a> PartialOrd for Search<'a> {
//     fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
//         let prob1 = self.prob();
//         let prob2 = other.prob();
//         match ((prob1 - prob2).abs() < 0.3, prob1 < prob2) {
//             (true, _) => match other.idx.partial_cmp(&self.idx) {
//                 Some(Ordering::Equal) => self.id.partial_cmp(&other.id),
//                 ord => ord,
//             },
//             (false, true) => Some(Ordering::Less),
//             (false, false) => Some(Ordering::Greater),
//         }
//     }
// }

// impl<'a> Clone for Search<'a> {
//     fn clone(&self) -> Self {
//         let mut id = ID.lock().unwrap();
//         let search = Self { id: *id, ..*self };
//         *id += 1;
//         search
//     }
// }

// impl<'a> Eq for Search<'a> {}

// impl<'a> Ord for Search<'a> {
//     fn cmp(&self, other: &Self) -> Ordering {
//         self.partial_cmp(other).unwrap()
//     }
// }

// impl LogMarkovTree {
//     pub fn get_bests(&self, sequence: Sequence, n: usize) -> impl Fn(Gene) -> i32 {
//         use Nucleotide::*;

//         #[cfg(debug_assertions)]
//         println!("{:?}", sequence.len());

//         let mut tree: BTreeSet<Search> = (0..sequence.len())
//             .map(|idx| {
//                 let mut search = Search::new(self);
//                 search.idx = idx as u16;
//                 search
//             })
//             .collect();
//         #[cfg(debug_assertions)]
//         println!("{:?}", tree);
//         let mut result: Vec<(Search, String)> = Vec::with_capacity(n);
//         for _ in 0..n {
//             if let Some(search_gene) = loop {
//                 if let Some(mut max) = tree.pop_last() {
//                     #[cfg(debug_assertions)]
//                     println!("{:?}", max);

//                     if max.is_final() {
//                         let gene = self.final_states.get(&max.vertice).unwrap().clone();
//                         break Some((max, gene));
//                     }
//                     let vertice = self[max.vertice];
//                     for &nucleotide in &[A, C, G, T] {
//                         let next_vertice = vertice[nucleotide];
//                         if next_vertice.0 != 0 {
//                             let mut search = max.clone();
//                             if search.idx < sequence.len() as u16 {
//                                 search.idx += 1;
//                                 search.vertice = next_vertice.0;
//                                 if sequence[max.idx as usize] == nucleotide {
//                                     search.last_muted = false;
//                                     search.matching += 1;
//                                 } else if search.last_muted {
//                                     search.mutation_in_a_row += 1;
//                                 } else {
//                                     search.last_muted = true;
//                                 }
//                             } else if search.last_muted {
//                                 search.mutation_in_a_row += 1;
//                             } else {
//                                 search.last_muted = true;
//                             }
//                             search.proba += next_vertice.1;
//                             search.total += 1;
//                             tree.insert(search);
//                         }
//                     }
//                     if self.final_states.get(&max.vertice).is_some() {
//                         let mut final_search = max.final_search(&sequence);
//                         final_search.proba += vertice.next[4].1;
//                         tree.insert(final_search);
//                     } else if max.idx < sequence.len() as u16 {
//                         max.total += 1;
//                         max.idx += 1;
//                         if max.last_muted {
//                             max.mutation_in_a_row += 1;
//                         }
//                         tree.insert(max);
//                     }
//                 } else {
//                     break None;
//                 }
//             } {
//                 result.push(search_gene);
//             } else {
//                 break;
//             }
//         }

//         result.sort_by_key(|x| x.0.clone());

//         let result: HashMap<String, usize> = result
//             .into_iter()
//             .enumerate()
//             .map(|(idx, search_gene)| (search_gene.1, idx))
//             .collect();
//         move |gene: Gene| *result.get(gene.name).unwrap_or(&0) as i32
//     }

//     pub fn predict(&self, sequence: Vec<Nucleotide>, n: usize) -> String {
//         use Nucleotide::*;

//         #[cfg(debug_assertions)]
//         println!("{:?}", sequence.len());

//         let mut tree: BTreeSet<Search> = (0..sequence.len())
//             .map(|idx| {
//                 let mut search = Search::new(self);
//                 search.idx = idx as u16;
//                 search
//             })
//             .collect();
//         #[cfg(debug_assertions)]
//         println!("{:?}", tree);
//         let mut result: Vec<(Search, String)> = Vec::with_capacity(n);
//         for _ in 0..n {
//             if let Some(search_gene) = loop {
//                 if let Some(mut max) = tree.pop_last() {
//                     #[cfg(debug_assertions)]
//                     println!("{:?}", max);

//                     if max.is_final() {
//                         let gene = self.final_states.get(&max.vertice).unwrap().clone();
//                         break Some((max, gene));
//                     }
//                     let vertice = self[max.vertice];
//                     for &nucleotide in &[A, C, G, T] {
//                         let next_vertice = vertice[nucleotide];
//                         if next_vertice.0 != 0 {
//                             let mut search = max.clone();
//                             if search.idx < sequence.len() as u16 {
//                                 search.idx += 1;
//                                 search.vertice = next_vertice.0;
//                                 if sequence[max.idx as usize] == nucleotide {
//                                     search.last_muted = false;
//                                     search.matching += 1;
//                                 } else if search.last_muted {
//                                     search.mutation_in_a_row += 1;
//                                 } else {
//                                     search.last_muted = true;
//                                 }
//                             } else if search.last_muted {
//                                 search.mutation_in_a_row += 1;
//                             } else {
//                                 search.last_muted = true;
//                             }
//                             search.proba += next_vertice.1;
//                             search.total += 1;
//                             tree.insert(search);
//                         }
//                     }
//                     if self.final_states.get(&max.vertice).is_some() {
//                         let mut final_search = max.final_search(&sequence);
//                         final_search.proba += vertice.next[4].1;
//                         tree.insert(final_search);
//                     } else if max.idx < sequence.len() as u16 {
//                         max.total += 1;
//                         max.idx += 1;
//                         if max.last_muted {
//                             max.mutation_in_a_row += 1;
//                         }
//                         tree.insert(max);
//                     }
//                 } else {
//                     break None;
//                 }
//             } {
//                 result.push(search_gene);
//             } else {
//                 break;
//             }
//         }

//         result.into_iter().max_by_key(|x| x.0.clone()).unwrap().1
//     }
// }

// #[derive(Clone)]
// pub struct MarkovTree {
//     vertices: Vec<Vertices>,
//     final_states: HashMap<u32, String>,
// }

// #[derive(Default, Clone, Copy)]
// pub struct Vertices {
//     next: [(u32, u32); 5],
// }

// #[derive(Debug, Default, Clone, Copy)]
// pub struct LogVertices {
//     next: [(u32, f32); 5],
// }

// impl Vertices {
//     fn new() -> Self {
//         Self::default()
//     }
// }

// impl MarkovTree {
//     pub fn from_genes_pattern(genes_pattern: &HashMap<(Vec<Nucleotide>, String), u32>) -> Self {
//         let mut markov_tree = MarkovTree::new();
//         for ((sequence, name), &n) in genes_pattern {
//             markov_tree.add_path(name.to_string(), 0, sequence, n);
//         }
//         markov_tree
//     }

//     pub fn into_log_markov_tree(
//         self,
//         cmp_proba: f64,
//         cmp_mut_rate: f64,
//         cmp_mut_row_rate: f64,
//     ) -> LogMarkovTree {
//         let MarkovTree {
//             mut vertices,
//             final_states,
//             ..
//         } = self;
//         vertices.iter_mut().for_each(|vertice| {
//             let sum: f64 = vertice.next.iter().map(|(_next, count)| count).sum::<u32>() as f64;
//             vertice
//                 .next
//                 .iter_mut()
//                 .filter(|(next, _)| !matches!(next, 0))
//                 .for_each(|(_, count)| {
//                     let log_proba = (*count as f64 / sum).ln();
//                     unsafe {
//                         *(count as *mut u32 as *mut f32) = log_proba as f32;
//                     }
//                 })
//         });

//         LogMarkovTree {
//             final_states,
//             vertices: unsafe { std::mem::transmute(vertices) },
//             cmp_proba,
//             cmp_mut_rate,
//             cmp_mut_row_rate,
//         }
//     }

//     fn new() -> Self {
//         Self {
//             vertices: vec![Vertices::new(); 1],
//             final_states: HashMap::new(),
//         }
//     }

//     fn add_path(&mut self, gene: String, origin: u32, sequence: &[Nucleotide], n: u32) {
//         use Nucleotide::*;
//         let mut vertice: u32 = origin;
//         let mut idx: usize = 0;
//         for &nucleotide in sequence {
//             if nucleotide == Nucleotide::N {
//                 for &nucleotide in &[A, C, G, T] {
//                     if self[vertice][nucleotide].0 == 0 {
//                         let new_vertice = self.new_vertice();
//                         self.link_vertices(vertice, new_vertice, nucleotide);
//                     }
//                     self[vertice][nucleotide].1 += n / 4 + 1;
//                     self.add_path(
//                         gene.clone(),
//                         self[vertice][nucleotide].0,
//                         &sequence[idx + 1..],
//                         n / 4 + 1,
//                     );
//                 }
//                 return;
//             }
//             let next_count = self[vertice][nucleotide];
//             if next_count.0 == 0 {
//                 break;
//             }
//             self[vertice][nucleotide].1 += n;
//             vertice = next_count.0;
//             idx += 1;
//         }

//         if idx < sequence.len() - 1 {
//             for &nucleotide in &sequence[idx..] {
//                 if nucleotide == Nucleotide::N {
//                     for &nucleotide in &[A, C, G, T] {
//                         let new_vertice = self.new_vertice();
//                         self.link_vertices(vertice, new_vertice, nucleotide);
//                         self[vertice][nucleotide].1 += n / 4 + 1;
//                         self.add_path(gene.clone(), new_vertice, &sequence[idx + 1..], n / 4 + 1);
//                     }
//                     return;
//                 }
//                 let next = self.new_vertice();
//                 self.link_vertices(vertice, next, nucleotide);
//                 self[vertice][nucleotide].1 += n;
//                 vertice = next;
//             }
//         }

//         self[vertice].next[4].1 += n;
//         let old = self.final_states.insert(vertice, gene);
//         if old.is_some() {
//             eprintln!(
//                 "WARNING : Several genes with the same sequence : {} and {}",
//                 old.unwrap(),
//                 self.final_states.get(&vertice).unwrap()
//             );
//         }
//     }

//     fn link_vertices(&mut self, v_src: u32, v_dest: u32, nucleotide: Nucleotide) {
//         self[v_src][nucleotide].0 = v_dest;
//     }

//     fn new_vertice(&mut self) -> u32 {
//         self.vertices.push(Vertices::new());
//         self.vertices.len() as u32 - 1
//     }
// }

// use std::ops::{Index, IndexMut};

// impl Index<Nucleotide> for Vertices {
//     type Output = (u32, u32);

//     fn index(&self, nucleotide: Nucleotide) -> &Self::Output {
//         &self.next[nucleotide.as_index()]
//     }
// }

// impl IndexMut<Nucleotide> for Vertices {
//     fn index_mut(&mut self, nucleotide: Nucleotide) -> &mut Self::Output {
//         &mut self.next[nucleotide.as_index()]
//     }
// }

// impl Index<u32> for MarkovTree {
//     type Output = Vertices;

//     fn index(&self, vertice: u32) -> &Self::Output {
//         &self.vertices[vertice as usize]
//     }
// }

// impl IndexMut<u32> for MarkovTree {
//     fn index_mut(&mut self, vertice: u32) -> &mut Self::Output {
//         &mut self.vertices[vertice as usize]
//     }
// }

// impl Index<Nucleotide> for LogVertices {
//     type Output = (u32, f32);

//     fn index(&self, nucleotide: Nucleotide) -> &Self::Output {
//         &self.next[nucleotide.as_index()]
//     }
// }

// impl IndexMut<Nucleotide> for LogVertices {
//     fn index_mut(&mut self, nucleotide: Nucleotide) -> &mut Self::Output {
//         &mut self.next[nucleotide.as_index()]
//     }
// }

// impl Index<u32> for LogMarkovTree {
//     type Output = LogVertices;

//     fn index(&self, vertice: u32) -> &Self::Output {
//         &self.vertices[vertice as usize]
//     }
// }

// impl IndexMut<u32> for LogMarkovTree {
//     fn index_mut(&mut self, vertice: u32) -> &mut Self::Output {
//         &mut self.vertices[vertice as usize]
//     }
// }

// impl Default for MarkovTree {
//     fn default() -> Self {
//         Self::new()
//     }
// }

// #[macro_export]
// macro_rules! log_markov_tree {
//     ($map:expr, {$($x: expr),* $(,)?}) => {
//         {
//             let mut genes_pattern = HashMap::new();
//             $(load_genes_pattern_from_simulations($x, $map, &mut genes_pattern);)*
//             MarkovTree::from_genes_pattern(&genes_pattern).into_log_markov_tree(0., 10., 5.)
//         }
//     };
// }
