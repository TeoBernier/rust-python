// use crate::prelude::*;

// pub struct KMersTreesConst<const K: usize>(HashMap<String, BTreeMap<usize, usize>>);

// impl<const K: usize> KMersTreesConst<K> {
//     pub fn construct_k_mers_trees(genes: &GenesList) -> KMersTrees {
//         let mut k_mers_trees: HashMap<String, BTreeMap<usize, usize>> = HashMap::new();

//         for &Gene { name, sequence } in genes.iter() {
//             let k_mers = Self::decompose_k_mers(sequence);
//             for (&k_mer, &count) in k_mers.iter() {
//                 if let Some(k_mers_tree) = k_mers_trees.get_mut(&k_mer) {
//                     if let Some(names) = k_mers_tree.get_mut(&count) {
//                         names.push(name);
//                     } else {
//                         k_mers_tree.insert(count, vec![name]);
//                     }
//                 } else {
//                     let mut k_mers_tree = BTreeMap::new();
//                     k_mers_tree.insert(count, vec![name]);
//                     k_mers_trees.insert(k_mer, k_mers_tree);
//                 }
//             }
//         }
//         KMersTrees(k_mers_trees)
//     }

//     pub fn decompose_k_mers(sequence: Sequence) -> HashMap<usize, usize> {
//         let mut k_mers = HashMap::new();
//         for i in K..sequence.0.len() {
//             let k_mer = sequence.slice(i - K..i);
//             if let Some(count) = k_mers.get_mut(&k_mer) {
//                 *count += 1;
//             } else {
//                 k_mers.insert(k_mer, 1);
//             }
//         }
//         k_mers
//     }

//     pub fn k_mers_scores_const<const RANGE: isize>(
//         &self,
//         sequence: Sequence,
//     ) -> impl Fn(Gene) -> i32 {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in Self::decompose_k_mers(sequence).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - RANGE) as usize..=count + RANGE as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         move |gene: Gene| *matches.get(gene.name).unwrap() as i32
//     }

//     pub fn k_mers_scores(&self, sequence: Sequence, range: usize) -> impl Fn(Gene) -> i32 {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in Self::decompose_k_mers(sequence).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - range) as usize..=count + range as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         move |gene: Gene| *matches.get(gene.name).unwrap() as i32
//     }

//     pub fn k_mers_best_const<const RANGE: isize>(&self, sequence: Sequence) -> String {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in Self::decompose_k_mers(sequence).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - RANGE) as usize..=count + RANGE as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         matches
//             .into_iter()
//             .max_by_key(|(_, score)| *score)
//             .unwrap()
//             .0
//     }

//     pub fn k_mers_best(&self, sequence: Sequence, range: usize) -> String {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in Self::decompose_k_mers(sequence).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - range) as usize..=count + range as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         matches
//             .into_iter()
//             .max_by_key(|(_, score)| *score)
//             .unwrap()
//             .0
//     }
// }

// pub struct KMersTrees(HashMap<String, BTreeMap<usize, usize>>, usize);

// impl KMersTrees {
//     pub fn construct_k_mers_trees(genes: &GenesList, n: usize) -> KMersTrees {
//         let mut k_mers_trees: HashMap<String, BTreeMap<usize, usize>> = HashMap::new();

//         for &Gene { name, sequence } in genes.iter() {
//             let k_mers = KMersTrees::decompose_k_mers(sequence, n);
//             for (&k_mer, &count) in k_mers.iter() {
//                 if let Some(k_mers_tree) = k_mers_trees.get_mut(&k_mer) {
//                     if let Some(names) = k_mers_tree.get_mut(&count) {
//                         names.push(name);
//                     } else {
//                         k_mers_tree.insert(count, vec![name]);
//                     }
//                 } else {
//                     let mut k_mers_tree = BTreeMap::new();
//                     k_mers_tree.insert(count, vec![name]);
//                     k_mers_trees.insert(k_mer, k_mers_tree);
//                 }
//             }
//         }
//         KMersTrees(k_mers_trees, n)
//     }

//     pub fn decompose_k_mers(sequence: Sequence, n: usize) -> HashMap<usize, usize> {
//         let mut k_mers = HashMap::new();
//         for i in n..sequence.0.len() {
//             let k_mer = sequence.slice(i - n..i);
//             if let Some(count) = k_mers.get_mut(&k_mer) {
//                 *count += 1;
//             } else {
//                 k_mers.insert(k_mer, 1);
//             }
//         }
//         k_mers
//     }

//     pub fn k_mers_scores_const<const RANGE: isize>(
//         &self,
//         sequence: Sequence,
//     ) -> impl Fn(Gene) -> i32 {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in KMersTrees::decompose_k_mers(sequence, self.1).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - RANGE) as usize..=count + RANGE as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         move |gene: Gene| *matches.get(gene.name).unwrap() as i32
//     }

//     pub fn k_mers_scores(&self, sequence: Sequence, range: usize) -> impl Fn(Gene) -> i32 {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in KMersTrees::decompose_k_mers(sequence, self.1).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - range) as usize..=count + range as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         move |gene: Gene| *matches.get(gene.name).unwrap() as i32
//     }

//     pub fn k_mers_best_const<const RANGE: isize>(&self, sequence: Sequence) -> String {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in KMersTrees::decompose_k_mers(sequence, self.1).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - RANGE) as usize..=count + RANGE as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         matches
//             .into_iter()
//             .max_by_key(|(_, score)| *score)
//             .unwrap()
//             .0
//     }

//     pub fn k_mers_best(&self, sequence: Sequence, range: usize) -> String {
//         let mut matches: HashMap<&str, usize> = HashMap::new();

//         for (k_mer, count) in KMersTrees::decompose_k_mers(sequence, self.1).into_iter() {
//             if let Some(k_mers_tree) = self.0.get(&k_mer) {
//                 for i in 0.max(count as isize - range) as usize..=count + range as usize {
//                     if let Some(names) = k_mers_tree.get(&i) {
//                         for name in names {
//                             if let Some(count) = matches.get_mut(name) {
//                                 *count += 1;
//                             } else {
//                                 matches.insert(name, 1);
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         matches
//             .into_iter()
//             .max_by_key(|(_, score)| *score)
//             .unwrap()
//             .0
//     }
// }
