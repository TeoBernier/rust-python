// use std::collections::HashSet;
// use std::time::Instant;
// use vdj_algo::prelude::*;

// fn main() {
//     let genes_j = load_genes_list_from_file("../data/database/IGHJgenes.txt");
//     let genes_d = load_genes_list_from_file("../data/database/IGHDgenes.txt");
//     let genes_v = load_genes_list_from_file("../data/database/IGHVgenes.txt");

//     let map_v = genes_v.get_map();
//     let map_d = genes_d.get_map();
//     let map_j = genes_j.get_map();

//     // let markov_tree = log_markov_tree!(&map_d, {
//     //         // "../data/database/ML/Data_VDJ/Real_data/sc1_BORJ_D_seq.txt",
//     //         // "../data/database/ML/Data_VDJ/Real_data/sc2_DUMJ_D_seq.txt",
//     //         // "../data/database/ML/Data_VDJ/Real_data/sc2_VERM_D_seq.txt",
//     //         // "../data/database/ML/Data_VDJ/Real_data/V100_RAVC_D_seq.txt",
//     //         // "../data/database/ML/Data_VDJ/Real_data/Vmut_KARG_D_seq.txt",
//     //         "../data/database/ML/Data_VDJ/Simulation/monoclonal_simp_indel_simu_D_seq.txt",
//     //         "../data/database/ML/Data_VDJ/Simulation/oligoclonal_simp_indel_simu_D_seq.txt",
//     //         "../data/database/ML/Data_VDJ/Simulation/polyclonal_simp_indel_simu_D_seq.txt",

//     // });

//     let (sequences, results) = load_sequences_from_file_with_mutations(
//         "../data/datasets/simulations/simple_plus_indels_80Mut_out.fasta",
//         &map_v,
//         &map_d,
//         &map_j,
//     );

//     // let sequences1 =
//     //     load_sequences_from_real_data_file("../data/database/ML/Data_V_seq/sc1_BORJ_V_seq.txt");
//     // let sequences2 =
//     //     load_sequences_from_real_data_file("../data/database/ML/Data_V_seq/sc2_VERM_V_seq.txt");
//     //     load_V_sequences_from_processed_file("../djeser_prog/fichiertraite.txt");
//     // let translation = load_translation_from_file("../djeser_prog/geneatrouver.txt");

//     // println!("{:?}", translation);

//     // let mut result = (0, 0);
//     // let mut result_without_allele = 0;

//     let k_mer_v = construct_k_mers_trees::<5>(&genes_v);

//     // let possible = genes_v
//     //     .iter()
//     //     .map(|gene| gene.name) //.split('*').next().unwrap())
//     //     .collect::<HashSet<_>>();
//     // let total = sequences1.len() + sequences2.len();
//     // let start = Instant::now();
//     // for (id, (res, sequence)) in sequences1.iter().chain(sequences2.iter()).enumerate() {
//     //     // print!("\r{:5} / {:5}", id, total);
//     //     let to_predict = res; // translation[results.get(name).unwrap().parse::<usize>().unwrap()].as_str();
//     //     if possible.contains(to_predict.as_str()) {
//     //         let prediction = //k_mers_best::<5, 2>(*sequence, &k_mer_v);
//     //         genes_v
//     //             .iter()
//     //             .map(|gene| (gene, match_n_sequence::<5>(gene.sequence, *sequence)))
//     //             .max_by_key(|(_, score)| *score)
//     //             .unwrap()
//     //             .0;
//     //         if prediction.name == to_predict {
//     //             result_without_allele += 1;
//     //             result.0 += 1;
//     //         } else if prediction .name
//     //             .split('*')
//     //             .next()
//     //             .unwrap()
//     //             == to_predict.split('*').next().unwrap()
//     //         {
//     //             result_without_allele += 1;
//     //         } else {
//     //             // println!(
//     //             //     "{} != {}",
//     //             //     prediction.name.split('*').next().unwrap(),
//     //             //     to_predict
//     //             // );
//     //         }
//     //         result.1 += 1;
//     //     }
//     // }
//     // let time = start.elapsed().as_secs_f64();
//     // //predictions.save_to("my_prediction.txt");
//     // println!("Time par séquence : {}", time / total as f64 * 1_000_000.);
//     // println!(
//     //     "Results with allele: {:.2}",
//     //     result.0 as f64 / result.1 as f64 * 100.
//     // );
//     // println!(
//     //     "Results without allele: {:.2}",
//     //     result_without_allele as f64 / result.1 as f64 * 100.
//     // );

//     // let sequences = load_sequences_from_file("../data/datasets/simulations/sim1.fasta");

//     // let results = SequenceResult::load_from_file(
//     //     "../data/datasets/simulations/simTrueGenes.txt",
//     //     &map_v,
//     //     &map_d,
//     //     &map_j,
//     // );

//     // let start = Instant::now();
//     // // let predictions = SequenceResult::calcul_with_k_mers::<5, 4, 5, 4, 2, 2>(
//     // //     &sequences, &genes_v, &genes_d, &genes_j,
//     // // );

//     let k_mer_d = construct_k_mers_trees::<4>(&genes_d);
//     let k_mer_j = construct_k_mers_trees::<4>(&genes_j);

//     let fv = |sequence| k_mers_scores::<5, 3>(sequence, &k_mer_v);
//     // // let fj = |sequence| alignment_scores(sequence, match_whole_sequence);
//     let fd = |sequence| alignment_scores(sequence, match_d_sequence);
//     // // let fd = |sequence| markov_tree.get_bests(sequence, 30);
//     // let fv = |sequence| k_mer_v.(sequence, match_n_sequence::<5>);
//     let fj = |sequence| alignment_scores(sequence, match_whole_sequence);
//     // let fd = |sequence| hmm_scores(sequence, &hmm_matrices);
//     // let fd = |sequence| markov_tree.get_bests(sequence, 30);

//     let predictions = SequenceResult::calcul_and_write_to(
//         &sequences,
//         &genes_v,
//         &genes_d,
//         &genes_j,
//         &results,
//         |sequence, genes_v, genes_d, genes_j| {
//             SequenceResult::get_best_by(sequence, genes_v, genes_d, genes_j, fv, fd, fj)
//         },
//         File::create("genes_alignment.txt").expect("Cannot open file"),
//     );
//     // let time = start.elapsed().as_secs_f64();
//     // //predictions.save_to("my_prediction.txt");

//     // println!(
//     //     "Time for 1 000 000 sequences : {}",
//     //     time / sequences.len() as f64 * 1_000_000.
//     // );
//     // //let predictions = SequenceResult::load_from("my_predictions_tmp.txt");

//     // predictions.save_to("my_predictions_tmp.txt");

//     predictions.compare_with(&results, "result_comparison.txt");

//     predictions.confusion_with(&results, "confusion_matrix.txt");

//     predictions.confusion_with_without_allele(&results, "confusion_matrix_without_allele.txt");

//     let confusion_matrixes = SequenceResult::get_confusion_matrixes_without_allele(
//         &predictions,
//         &results,
//         &genes_v,
//         &genes_d,
//         &genes_j,
//     );
//     let conf_v = DiffList::from_conf(&confusion_matrixes.0);
//     let conf_d = DiffList::from_conf(&confusion_matrixes.1);
//     let conf_j = DiffList::from_conf(&confusion_matrixes.2);

//     // println!("{}\n\n{}\n\n{}", conf_v, conf_d, conf_j);

//     // println!("{:?}\n\n{:?}\n\n{:?}", conf_v, conf_d, conf_j);

//     use std::fs::File;
//     use std::io::Write;

//     write!(
//         File::create("accuracy.txt").expect("Cannot create file"),
//         "{}\n\n{}\n\n{}",
//         conf_v,
//         conf_d,
//         conf_j
//     )
//     .expect("Cannot write to file");

//     write!(
//         File::create("accuracy_most_choose.txt").expect("Cannot create file"),
//         "{:?}\n\n{:?}\n\n{:?}",
//         conf_v,
//         conf_d,
//         conf_j
//     )
//     .expect("Cannot write to file");
// }

fn main() {}
