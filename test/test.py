from rust_vdj import PyGenesList

gene_list = PyGenesList("./data/database/IGHVgenes.txt")
pylist = gene_list.as_list()
print(list)
seq = "ACCGTCCAGTCAGT"
print(gene_list.alignement_whole_sequence(seq))
print("\n\n")

gene = gene_list.alignement_with_n_decalage_sequence(seq, 10)
print(gene)
print(gene_list.alignement_with_10_decalage_sequence(seq))
print(seq)
print(list(filter(lambda x: x[0] == gene, pylist))[0][1])
print("\n\n")

gene = gene_list.alignement_with_n_decalage_sequence(seq, 5)
print(gene)
print(gene_list.alignement_with_5_decalage_sequence(seq))
print(seq)
print(list(filter(lambda x: x[0] == gene, pylist))[0][1])
print("\n\n")