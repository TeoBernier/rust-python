use cpython::*;
use prelude::*;
use std::cell::RefCell;
use std::collections::HashMap;
use std::ops::*;
use vdj_algo::prelude::{self, GenesList};

py_module_initializer!(rust_vdj, |py, m| {
    m.add(py, "__doc__", "Module documentation string")?;
    // m.add_class::<PyMarkovTree>(py)?;
    // m.add_class::<PatternParser>(py)?;
    m.add_class::<PyGenesList>(py)?;

    Ok(())
});

// py_class!(class PyMarkovTree |py| {
//     data markov_tree: LogMarkovTree;

//     def __new__(_cls, patterns: PyObject, proba: f64, mut_rate: f64, mut_row_rate: f64) -> PyResult<PyMarkovTree> {
//         PyMarkovTree::create_instance(py, MarkovTree::from_genes_pattern(&patterns.cast_into::<PatternParser>(py)?.patterns(py).borrow()).into_log_markov_tree(proba, mut_rate, mut_row_rate))
//     }

//     def __str__(&self) -> PyResult<String> {
//         Ok(format!("{:?}", self.markov_tree(py)))
//     }

//     def predict(&self, sequence: &str, n: usize) -> PyResult<String> {
//         let sequence = Sequence::vec_from_chars(sequence);
//         Ok(self.markov_tree(py).predict(sequence, n))
//     }
// });

// py_class!(class PatternParser |py| {
//     data patterns: RefCell<HashMap<(Vec<Nucleotide>, String), u32>>;
//     def __new__(_cls) -> PyResult<PatternParser> {
//         PatternParser::create_instance(py, RefCell::new(HashMap::<(Vec<Nucleotide>, String), u32>::new()))
//     }

//     def __str__(&self) -> PyResult<String> {
//         use std::ops::Deref;
//         let borrow = self.patterns(py).borrow();
//         let mut map = borrow.deref().iter().map(|((seq, name), count)| (name, seq.iter().map(|&n| Into::<char>::into(n)).collect::<String>(), count)).map(|tuple| format!("{}, {}, {}", tuple.0, tuple.1, tuple.2)).collect::<Vec<_>>();
//         map.sort();
//         Ok(format!("{:#?}", map))
//     }

//     def as_dict(&self) -> PyResult<PyDict> {
//         use std::ops::Deref;
//         let dict = PyDict::new(py);
//         for ((sequence, name), count) in self.patterns(py).borrow().deref() {
//             dict.set_item(py, PyTuple::new(py, &[name.to_py_object(py).into_object(), sequence.iter().map(|&n| Into::<char>::into(n)).collect::<String>().to_py_object(py).into_object()]), count)?;
//         }
//         Ok(dict)
//     }

//     def train(&self, file: &str) -> PyResult<PyObject> {
//         load_pattern_from_simulations(file, &mut self.patterns(py).borrow_mut());
//         Ok(py.None())
//     }

//     def markov_tree(&self, proba: f64, mut_rate: f64, mut_row_rate: f64) -> PyResult<PyMarkovTree> {
//         PyMarkovTree::create_instance(py, MarkovTree::from_genes_pattern(&self.patterns(py).borrow()).into_log_markov_tree(proba, mut_rate, mut_row_rate))
//     }
// });

py_class!(class PyGenesList |py| {
    data genes_list : GenesList;

    def __new__(_cls, path: &str) -> PyResult<PyGenesList> {
        PyGenesList::create_instance(py, load_genes_list_from_file(path))
    }

    def as_list(&self) -> PyResult<PyList> {
        let list = PyList::new(py, &[]);
        for Gene{sequence, name} in &self.genes_list(py).0 {
            list.append(py, PyTuple::new(py, &[name.to_py_object(py).into_object(), sequence.iter().map(|&n| Into::<char>::into(n)).collect::<String>().to_py_object(py).into_object()]).into_object());
        }
        Ok(list)
    }

    def alignement_whole_sequence(&self, sequence: &str) -> PyResult<String> {
        let sequence = Sequence::new_nucleotides(sequence);
        Ok(self.genes_list(py).0.iter().map(|Gene{name, sequence: gene}| (match_whole_sequence(Sequence::from(&gene),  Sequence::from(&sequence)), name)).max().unwrap().1.clone())
    }

    def alignement_with_n_decalage_sequence(&self, sequence: &str, n:usize) -> PyResult<String> {
        let sequence = Sequence::new_nucleotides(sequence);
        Ok(self.genes_list(py).0.iter().map(|Gene{name, sequence: gene}| (match_n_sequence_param(Sequence::from(&gene),  Sequence::from(&sequence), n), name)).max().unwrap().1.clone())
    }

    def alignement_with_5_decalage_sequence(&self, sequence: &str) -> PyResult<String> {
        let sequence = Sequence::new_nucleotides(sequence);
        Ok(self.genes_list(py).0.iter().map(|Gene{name, sequence: gene}| (match_n_sequence::<5>(Sequence::from(&gene),  Sequence::from(&sequence)), name)).max().unwrap().1.clone())
    }

    def alignement_with_10_decalage_sequence(&self, sequence: &str) -> PyResult<String> {
        let sequence = Sequence::new_nucleotides(sequence);
        Ok(self.genes_list(py).0.iter().map(|Gene{name, sequence: gene}| (match_n_sequence::<10>(Sequence::from(&gene),  Sequence::from(&sequence)), name)).max().unwrap().1.clone())
    }
});
